Alternating rectangle function:
===============================
This directory contains a programme which plots
the sprectrum of an alternating rectangle function.

Formula:
--------
The spectrum is described by the following function:

F(jw) = (2'PI'*T)/P * SUM_n[ si(nw_0T / 2) * delta(w - nw_0)  
where 'PI' is the constant pi, T is the widht of the rectangle,
P is the periodicity of the alternating rectangles and w_0 is 2'PI'/P

Implementation:
---------------
As the function F(jw) has the shape of a Sha-Function which is merely a
discreet function the Amplituted provided by the si-Functio and the positions
in the frequency domain provided by the sha-Function will be calculated
seperatly.
