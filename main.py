'''
Calculate and plot the spectrum of an alternating rectangle function
'''
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, CheckButtons


def si(x):
    '''
    Calculate the si aka sin(x)/x

    :param x: The argument x for sin(x)/x
    :return: The value of si at x
    '''
    if x == 0:
        return 1.0
    return np.sin(x) / x


def amplitude(n, T, P):
    '''
    Calculate the amplitudes of the nth delta impuls in respect
    to the parameters P and T.

    :param n: The index of the delta impuls to get the amplitude for
    :param T: The width of the rectangle functions
    :param P: The periodicity of the alternating rectangles
    :return: The amplitude of the nth delta impuls
    '''
    w_0 = (2.0 * np.pi) / P
    return si((n * w_0 * T) / 2.0)


def frequency(n, P):
    '''
    Get the frequency aka the position in the frequency domain of the
    nth delta impuls

    :param n: The index of the delta impulse to get the frequency for
    :param P: The periodicity of the alternating rectangles
    :return: The frequency for the nth delta impuls
    '''
    return n * ((2.0 * np.pi) / P)


if __name__ == '__main__':
    # The halfe resolution to work in
    half_resolution = 1000
    # The parameters T for the width of the rectangle and
    # P for the periodicity of the alternating rectangle function
    T = 1
    P = 2 * T
    # A flag if the duty cycle is locked to be 50% or not. If it is, P has
    # to be two times T
    duty50_locked = False
    # The position in freqency domain and the amplitudes of the delta impulses
    frequencies = []
    amplitudes = []
    # Create a new subplot
    fig, axis = plt.subplots()
    # Adjust the main window to be left aligned
    plt.subplots_adjust(left=0.025, right=0.75, top=0.9)

    def plot():
        '''
        Just plot the current data
        '''
        axis.clear()
        axis.plot(frequencies, amplitudes)
        fig.canvas.draw_idle()

    # The update_t_slider function has to be defined here so it
    # can access all neccessary functions
    def update_t_slider(val):
        '''
        Update the value T and recalculate the plot
        '''
        global T, P, frequencies, amplitudes
        T = val
        if duty50_locked:
            # If the duty cycle is locked to be 50% set P to be two times T
            # as this represents a duty cycle of 50%
            P = 2.0 * T
            p_slider.set_val(P)
        frequencies = []
        amplitudes = []
        for i in range(-half_resolution, half_resolution):
            frequencies.append(frequency(i, P))
            amplitudes.append(amplitude(i, T, P))
        plot()

    def update_p_slider(val):
        '''
        Update the value P and recalculate the plot
        '''
        global T, P, frequencies, amplitudes
        P = val
        frequencies = []
        amplitudes = []
        for i in range(-half_resolution, half_resolution):
            frequencies.append(frequency(i, P))
            amplitudes.append(amplitude(i, T, P))
        plot()

    def update_r_slider(val):
        '''
        Update the value half_resolution and recalculate the plot
        '''
        global T, P, frequencies, amplitudes, half_resolution
        half_resolution = int(val)
        frequencies = []
        amplitudes = []
        for i in range(-half_resolution, half_resolution):
            frequencies.append(frequency(i, P))
            amplitudes.append(amplitude(i, T, P))
        plot()

    def update_duty50_rbutton(label):
        '''
        Just toggle the duty50 flag which will be used in the update_t_slider
        If the duty cycle is locked to 50% the periodicity P alway has to
        be twice as long as T.
        '''
        global duty50_locked
        duty50_locked = not duty50_locked

    # Create an additional axis to render the t parameter slider
    t_parameter_axis = plt.axes([0.8, 0.86, 0.2 - 0.025, 0.04])

    # Create an additional axis to render the p parameter slider
    p_parameter_axis = plt.axes([0.8, 0.78, 0.2 - 0.025, 0.04])

    # Create an additional axis to render the resolution parameter slider
    r_parameter_axis = plt.axes([0.8, 0.70, 0.2 - 0.025, 0.04])

    # Create an additional axis to render the duty cycle radio button
    duty50_rbutton_axis = plt.axes([0.8, 0.62, 0.2 - 0.05, 0.04])

    # Create a slider widget for t
    t_slider = Slider(t_parameter_axis, 'Rect Width', 0.01, 1,
        valinit=T, valstep=0.01)
    # Create a slider widget for p
    p_slider = Slider(p_parameter_axis, 'Periode', 0.01, 1,
        valinit=P, valstep=0.01)
    # Create a slider widget for p
    r_slider = Slider(r_parameter_axis, 'Resolut.', 10, 1000,
        valinit=half_resolution, valstep=1)
    # Create a duty cycle 50% radio button
    duty50_rbutton = CheckButtons(duty50_rbutton_axis, ('Duty50%', ))

    # Set the corresponding callback functions
    t_slider.on_changed(update_t_slider)
    p_slider.on_changed(update_p_slider)
    r_slider.on_changed(update_r_slider)
    duty50_rbutton.on_clicked(update_duty50_rbutton)

    # Initially generate the plot
    update_t_slider(T)
    plot()
    # Show the plot (which also starts the loop for the gui elements)
    plt.show()
